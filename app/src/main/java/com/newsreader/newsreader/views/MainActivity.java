package com.newsreader.newsreader.views;

import android.Manifest;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.newsreader.newsreader.R;
import com.newsreader.newsreader.data.NewsAdapter;
import com.newsreader.newsreader.data.Article;
import com.newsreader.newsreader.presenters.NewsPresenter;

import java.util.List;

public class MainActivity extends BaseActivity implements NewsPresenter.NewsView {
    private final int JSON_REQUEST_CODE = 17;
    private final int XML_REQUEST_CODE = 18;

    private RecyclerView newsList;
    private SwipeRefreshLayout refreshLayout;
    private NewsAdapter adapter;
    private NewsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initUI();
    }

    private void initViews() {
        newsList = findViewById(R.id.news_list);
        refreshLayout = findViewById(R.id.refresh_layout);
    }

    private void initUI() {
        newsList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NewsAdapter();
        newsList.setAdapter(adapter);
        presenter = new NewsPresenter(this);

        adapter.setOnNewsItemClickListener(article -> {
            Intent intent = new Intent(MainActivity.this, WebActivity.class);
            intent.putExtra(WebActivity.URL_ARGUMENT, article.getArticleUrl());
            startActivity(intent);
        });

        refreshLayout.setOnRefreshListener(() -> {
            refreshLayout.setRefreshing(false);
            presenter.updateNews();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    item.getItemId() == R.id.export_json ? JSON_REQUEST_CODE : XML_REQUEST_CODE);
        } else {
            switch (item.getItemId()) {
                case R.id.export_json:
                    presenter.saveJson();
                    return true;
                case R.id.export_xml:
                    presenter.saveXml();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == JSON_REQUEST_CODE || requestCode == XML_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == JSON_REQUEST_CODE) {
                    presenter.saveJson();
                } else {
                    presenter.saveXml();
                }
            } else {
                Toast.makeText(this, R.string.grant_permission, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public LifecycleOwner getLifeCycleOwner() {
        return this;
    }

    @Override
    public void addArticles(List<Article> articles) {
        adapter.addNews(articles);
    }

    @Override
    public void showLoading() {
        showLoadingDialog();
    }

    @Override
    public void showError(String message) {
        showErrorDialog(message);
    }

    @Override
    public void hideDialogs() {
        dismissCurrentDialog();
    }

    @Override
    public void showToast(int stringRes) {
        Toast.makeText(this, stringRes, Toast.LENGTH_SHORT).show();
    }
}
