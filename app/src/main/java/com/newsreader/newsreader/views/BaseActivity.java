package com.newsreader.newsreader.views;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.newsreader.newsreader.R;
import com.newsreader.newsreader.Utils;

public abstract class BaseActivity extends AppCompatActivity {

    public static final String LOADING_TAG = "loading";

    private LoadingDialogFragment loadingDialog;

    private int pendingLoadingCount = 0;


    public void showLoadingDialog() {
        if (pendingLoadingCount == 0) {
            dismissCurrentDialog();
            loadingDialog = LoadingDialogFragment.newInstance();
            loadingDialog.show(getSupportFragmentManager(), LOADING_TAG);
        }
        pendingLoadingCount++;
    }

    public void dismissCurrentDialog() {
        if (getSupportFragmentManager().findFragmentByTag(LOADING_TAG) != null) {
            if (pendingLoadingCount > 0) {
                pendingLoadingCount--;
            }
            if (pendingLoadingCount == 0) {
                loadingDialog.dismiss();
            }
        }
    }

    public void showErrorDialog(String errorMsg) {
        dismissCurrentDialog();
        String errorMessage = Utils.isNetworkConnected(this)
                ? (errorMsg != null && !errorMsg.isEmpty()) ? errorMsg : getString(R.string.default_error_message)
                : getString(R.string.no_connection_error_message);
        new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(errorMessage)
                .setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.dismiss())
                .create().show();
    }
}
