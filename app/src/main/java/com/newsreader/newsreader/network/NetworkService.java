package com.newsreader.newsreader.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class NetworkService {
    private static NetworkService instance;
    private final String NEWS_BASE_URL = "https://newsapi.org";
    private final String BBC_NEWS_BASE_URL = "http://feeds.bbci.co.uk";
    private NewsService newsService;
    private BBCNewsService bbcNewsService;

    private NetworkService() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        newsService = new Retrofit.Builder()
                .baseUrl(NEWS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .build().create(NewsService.class);

        bbcNewsService = new Retrofit.Builder()
                .baseUrl(BBC_NEWS_BASE_URL)
                .client(client.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build().create(BBCNewsService.class);
    }

    public static NetworkService getInstance() {
        if (instance == null) {
            instance = new NetworkService();
        }
        return instance;
    }

    public NewsService getNewsService() {
        return newsService;
    }

    public BBCNewsService getBbcNewsService() {
        return bbcNewsService;
    }
}
