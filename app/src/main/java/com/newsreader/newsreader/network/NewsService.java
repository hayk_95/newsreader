package com.newsreader.newsreader.network;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {
    @GET("/v2/top-headlines")
    Observable<Response<APIResponse>> getArticles(@Query("sources") String source, @Query("apiKey") String apiKey);
}