package com.newsreader.newsreader.network;

import com.google.gson.annotations.SerializedName;
import com.newsreader.newsreader.data.Article;

import java.util.ArrayList;

public class APIResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("totalResults")
    private int count;

    @SerializedName("articles")
    private ArrayList<Article> articles = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }
}
