package com.newsreader.newsreader.network;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface BBCNewsService {
    @GET("/news/video_and_audio/technology/rss.xml")
    Observable<RssFeed> getFeeds();
}
