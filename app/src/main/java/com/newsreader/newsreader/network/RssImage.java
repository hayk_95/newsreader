package com.newsreader.newsreader.network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "image", strict = false)
public class RssImage
{
    @Element
    public String url;
}
