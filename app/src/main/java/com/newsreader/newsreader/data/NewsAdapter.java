package com.newsreader.newsreader.data;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.newsreader.newsreader.R;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> {

    private ArrayList<Article> items = new ArrayList<>();
    private NewsItemClickListener listener;

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new NewsHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_holder_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder newsHolder, int i) {
        newsHolder.title.setText(items.get(i).getTitle());
        newsHolder.desc.setText(items.get(i).getDescription());
        Glide.with(newsHolder.itemView)
                .load(items.get(i).getThumb())
                .into(newsHolder.thumb);
        newsHolder.itemView.setOnClickListener(v -> {
            if(listener != null){
                listener.onItemClicked(items.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addNews(List<Article> news){
        items.clear();
        items.addAll(news);
        notifyDataSetChanged();
    }

    public interface NewsItemClickListener {
        void onItemClicked(Article article);
    }

    public void setOnNewsItemClickListener(NewsItemClickListener listener){
        this.listener = listener;
    }

    class NewsHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView desc;
        ImageView thumb;

        NewsHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.article_title);
            desc = itemView.findViewById(R.id.article_desc);
            thumb = itemView.findViewById(R.id.article_thumb);
        }
    }
}
