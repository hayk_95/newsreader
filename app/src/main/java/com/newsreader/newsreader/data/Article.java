package com.newsreader.newsreader.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Article {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("urlToImage")
    private String thumb;
    @SerializedName("url")
    private String articleUrl;
    @SerializedName("publishedAt")
    private String textDate;
    private Date date;

    public Article(String title, String description, String thumb, String articleUrl, String textDate) {
        this.title = title;
        this.description = description;
        this.thumb = thumb;
        this.articleUrl = articleUrl;
        this.textDate = textDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getThumb() {
        return thumb;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public String getTextDate() {
        return textDate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
