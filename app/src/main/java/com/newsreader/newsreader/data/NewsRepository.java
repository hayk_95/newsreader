package com.newsreader.newsreader.data;

import com.newsreader.newsreader.network.APIResponse;
import com.newsreader.newsreader.network.NewsService;
import com.newsreader.newsreader.network.NetworkService;
import com.newsreader.newsreader.network.RssFeed;
import com.newsreader.newsreader.network.RssItem;
import com.newsreader.newsreader.network.BBCNewsService;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class NewsRepository {
    private final String NEWS_SOURCE = "techcrunch";
    private final String NEWS_API_KEY = "97ba815035ae4381b223377b2df975ab";

    private NewsService newsService;
    private BBCNewsService BBCNewsService;

    public NewsRepository() {
        newsService = NetworkService.getInstance().getNewsService();
        BBCNewsService = NetworkService.getInstance().getBbcNewsService();
    }

    public Observable<ArrayList<Article>> getArticles() {

        Observable<Response<APIResponse>> responseOneObservable = newsService.getArticles(NEWS_SOURCE, NEWS_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Observable<RssFeed> responseTwoObservable = BBCNewsService.getFeeds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturnItem(new RssFeed());


        return Observable.zip(responseOneObservable, responseTwoObservable, (apiResponseResponse, rssFeed) -> {
            ArrayList<Article> articles = new ArrayList<>();
            if (apiResponseResponse.isSuccessful() && apiResponseResponse.body() != null && apiResponseResponse.body().getStatus().equals("ok")) {
                articles.addAll(apiResponseResponse.body().getArticles());
            }
            if(rssFeed.channel != null) {
                for (RssItem item : rssFeed.channel.item) {
                    articles.add(new Article(item.title, item.description, rssFeed.channel.image.url, item.link, item.pubDate));
                }
            }
            return articles;
        });
    }
}
