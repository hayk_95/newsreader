package com.newsreader.newsreader.presenters;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Environment;
import android.util.Xml;

import com.newsreader.newsreader.R;
import com.newsreader.newsreader.data.Article;
import com.newsreader.newsreader.data.NewsRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NewsPresenter implements LifecycleObserver {

    private static final String FOLDER_NAME = "/NewsReader";
    private static final String JSON_FILE_NAME = FOLDER_NAME + "/news_reader.json";
    private static final String XML_FILE_NAME = FOLDER_NAME + "/news_reader.xml";
    private final String ARTICLES_KEY = "articles";
    private final String TITLE_KEY = "title";
    private final String DESCRIPTION_KEY = "description";
    private final String URL_KEY = "url";
    private final String PUB_DATE_KEY = "pubDate";
    private final String THUMB_KEY = "thumb";

    private NewsView view;
    private NewsRepository repository;
    private ArrayList<Article> articles = new ArrayList<>();
    private FileWriter fileWriter;
    private File file;

    public NewsPresenter(NewsView view) {
        this.view = view;
        repository = new NewsRepository();
        view.getLifeCycleOwner().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onViewCreated() {
        getArticles();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onViewDestroy() {
        view = null;
    }

    private void getArticles() {
        repository.getArticles().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Article>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        view.showLoading();
                    }

                    @Override
                    public void onNext(ArrayList<Article> articles) {
                        view.hideDialogs();
                        sortArticles(articles);
                        NewsPresenter.this.articles.addAll(articles);
                        view.addArticles(articles);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void sortArticles(ArrayList<Article> articles) {
        for (Article article : articles) {
            Date parsed;
            SimpleDateFormat format;
            try {
                format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",Locale.ENGLISH);
                parsed = format.parse(article.getTextDate());
                article.setDate(parsed);
            } catch (ParseException pe) {
                format = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
                try {
                    parsed = format.parse(article.getTextDate());
                    article.setDate(parsed);
                } catch (ParseException e) {
                    e.printStackTrace();
                    throw new IllegalArgumentException(e);
                }
            }
        }
        Collections.sort(articles, (o1, o2) -> o1.getDate().after(o2.getDate()) ? -1 : o1.getDate().equals(o2.getDate()) ? 0 : 1);
    }

    public void updateNews(){
        getArticles();
    }

    public void saveJson() {
        if (isFolderExist()) {
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            Map<String, Object> map = new HashMap<>();
            for (Article article : articles) {
                map.put(THUMB_KEY, article.getThumb());
                map.put(PUB_DATE_KEY, article.getTextDate());
                map.put(URL_KEY, article.getArticleUrl());
                map.put(DESCRIPTION_KEY, article.getDescription());
                map.put(TITLE_KEY, article.getTitle());
                jsonArray.put(new JSONObject(map));
            }
            try {
                jsonObject.put(ARTICLES_KEY, jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            file = new File(Environment.getExternalStorageDirectory().toString() + JSON_FILE_NAME);
            try {
                fileWriter = new FileWriter(file.getAbsoluteFile());
                BufferedWriter writer = new BufferedWriter(fileWriter);
                writer.write(jsonObject.toString());
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            view.showToast(R.string.save_success);
        }
    }

    public void saveXml() {
        if (isFolderExist()) {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", ARTICLES_KEY);
                serializer.attribute("", "number", String.valueOf(articles.size()));
                for (Article msg : articles) {
                    serializer.startTag("", "article");
                    serializer.startTag("", TITLE_KEY);
                    serializer.text(msg.getTitle());
                    serializer.endTag("", TITLE_KEY);
                    serializer.startTag("", DESCRIPTION_KEY);
                    serializer.text(msg.getDescription());
                    serializer.endTag("", DESCRIPTION_KEY);
                    serializer.startTag("", URL_KEY);
                    serializer.text(msg.getArticleUrl());
                    serializer.endTag("", URL_KEY);
                    serializer.startTag("", PUB_DATE_KEY);
                    serializer.text(msg.getTextDate());
                    serializer.endTag("", PUB_DATE_KEY);
                    serializer.startTag("", THUMB_KEY);
                    serializer.text(msg.getThumb());
                    serializer.endTag("", THUMB_KEY);
                    serializer.endTag("", "article");
                }
                serializer.endTag("", ARTICLES_KEY);
                serializer.endDocument();

                file = new File(Environment.getExternalStorageDirectory().toString() + XML_FILE_NAME);
                fileWriter = new FileWriter(file.getAbsoluteFile());
                BufferedWriter wr = new BufferedWriter(fileWriter);
                wr.write(writer.toString());
                wr.close();
                view.showToast(R.string.save_success);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private boolean isFolderExist() {
        String folder = Environment.getExternalStorageDirectory() + FOLDER_NAME;
        File file = new File(folder);
        if (!file.exists()) {
            return file.mkdir();
        } else {
            return true;
        }
    }

    public interface NewsView {
        LifecycleOwner getLifeCycleOwner();

        void addArticles(List<Article> articles);

        void showLoading();

        void showError(String message);

        void hideDialogs();

        void showToast(int stringRes);
    }
}
